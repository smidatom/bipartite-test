#include <iostream>
#include <queue>
#include <climits>

using namespace std;

const int FRESH = -1;
const int RED = 0;
const int BLACK = 1;

struct Node
{	
	int status;
	int successors[1000];
	int sucCnt;
};

Node * nodes = new Node[200];

bool bfs(int start, Node * nodes)
{
	Node * tmp;
	nodes[start].status = BLACK;

	queue<Node*> qu;
	qu.push(&nodes[start]);
	while(!qu.empty())
	{
		tmp = qu.front();
		qu.pop();
		for(int i=0; i<tmp->sucCnt; i++)
		{
			if(nodes[tmp->successors[i]].status == FRESH)
			{
				nodes[tmp->successors[i]].status = !tmp->status;
				qu.push(&nodes[tmp->successors[i]]);
			}
			else if(nodes[tmp->successors[i]].status == tmp->status)
			{
				return false;
			}
		}
	}
	return true;
}

void solveProblem(int nodesCnt)
{
	int edges, from, to;
	cin >> edges;

	for(int i=0; i<nodesCnt; i++)
	{
		nodes[i].status = FRESH;
		nodes[i].sucCnt = 0;
	}

	for(int i=0; i<edges; i++)
	{
		cin >> from >> to;
		nodes[from].successors[nodes[from].sucCnt] = to;
		nodes[from].sucCnt++;
		nodes[to].successors[nodes[to].sucCnt] = from;
		nodes[to].sucCnt++;
	}

	for(int i=0; i<nodesCnt; i++)
		if(nodes[i].status==FRESH)
			if(!bfs(i, nodes))
			{
				cout << "NO" << endl;
				return;
			}	
	cout << "YES" << endl;
	return;
}

int main()
{
	int n;
	cin >> n;
	while(n)
	{
		solveProblem(n);
		cin >> n;
	}
	return 0;
}